// "We need to add an event listener for when the DOM loads."
// "Let's declare a variable that will hold the URL for the API that we just created."
// "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
// "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."



window.addEventListener('DOMContentLoaded',async () => {

try{
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        // console.log(data);

        const select_el = document.querySelector('#state');

        for (let state of data.states) {
            let option = document.createElement('option');
            option.innerHTML = state.name;
            option.value = state.abbreviation;
            // console.log(option)

            select_el.appendChild(option);
        }

        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async (event) => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            // console.log(json);

            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
            },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation);
            }

        });

    }
} catch (e) {
    console.error(e);
}
});
