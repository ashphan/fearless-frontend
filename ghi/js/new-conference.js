window.addEventListener('DOMContentLoaded', async () =>{

    try {
        const url = 'http://localhost:8000/api/locations/'
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            // console.log(data);

            const select_el = document.getElementById('location');
            // console.log(select_el);

            for (let location of data.locations) {
                let option = document.createElement('option');
                option.innerHTML = location.name;
                option.value = location.id;
                // console.log(option);

                select_el.appendChild(option);
            }

            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                // console.log(json);

                const conferenceURL = 'http://localhost:8000/api/conferences/'
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                },
                };
                const response = await fetch(conferenceURL, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newConference = await response.json();
                    console.log(newConference);
                }



            })
        }


} catch (e) {
    console.error(e);
}
});
