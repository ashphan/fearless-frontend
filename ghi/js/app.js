function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class = "col-4">
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        <div class="card-footer">${start} - ${end}</div>
        </div>
      </div>
    </div>
    `;
  }



window.addEventListener('DOMContentLoaded',async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        console.log(response);

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);

        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailURL = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description
                    const pictureURL = details.conference.location.picture_url;
                    const startdate = new Date(details.conference.starts);
                    const start = startdate.toLocaleDateString();
                    const enddate = new Date(details.conference.ends);
                    const end = enddate.toLocaleDateString();
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureURL, start, end, location);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }
            }



        }
    } catch (e) {
        console.error('An error occured', e);
    }


});
